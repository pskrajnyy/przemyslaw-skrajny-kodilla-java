package com.kodilla.exception.test;

import java.util.HashMap;

public class FlightLogic {

    public boolean findFlight(Flight flight) throws RouteNotFoundException {
        HashMap<String,Boolean> airports = new HashMap<>();
        airports.put("Warszawa",true);
        airports.put("Gdańsk",true);
        airports.put("Zakopane",true);
        airports.put("Szczecin",false);
        airports.put("Gdynia",false);

        String airport = flight.getArrivalAirport();
        if(airports.containsKey(airport)) {
            return airports.get(airport);
        } else {
            throw new RouteNotFoundException();
        }
    }
}
