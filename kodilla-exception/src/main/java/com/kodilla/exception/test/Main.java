package com.kodilla.exception.test;

public class Main {

    public static void main(String[] args) {
        Flight flight1 = new Flight("Szczecin","Gdańsk");
        Flight flight2 = new Flight("Warszawa","Szczecin");
        Flight flight3 = new Flight("New York","Deblin");


        FlightLogic flightLogic = new FlightLogic();
        System.out.println("Czy istnieje możliwość lotu na dane lotnisko?");
        try{
            System.out.println(flight1.getArrivalAirport() + " " + flightLogic.findFlight(flight1));
            System.out.println(flight2.getArrivalAirport() + " " + flightLogic.findFlight(flight2));
            System.out.println(flight3.getArrivalAirport() + " " + flightLogic.findFlight(flight3));
        } catch (RouteNotFoundException e) {
            System.out.println("Cos poszlo nie tak: " + e);
        }
    }
}