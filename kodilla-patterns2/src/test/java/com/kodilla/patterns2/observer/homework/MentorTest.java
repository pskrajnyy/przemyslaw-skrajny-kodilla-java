package com.kodilla.patterns2.observer.homework;

import org.junit.Test;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class MentorTest {

    @Test
    public void testUpdate() {
        //Given
        Student johnSmith = new Student("John Smith");
        Student adamWest = new Student("Adam West");
        Student annaNowak = new Student("Anna Nowak");

        Mentor janKowalski = new Mentor("Jan Kowalski");
        Mentor robertKubica = new Mentor("Robert Kubica");

        johnSmith.registerObserver(janKowalski);
        adamWest.registerObserver(janKowalski);
        johnSmith.registerObserver(robertKubica);
        adamWest.registerObserver(robertKubica);
        annaNowak.registerObserver(robertKubica);

        //When
        johnSmith.addTask("Queue Task");
        adamWest.addTask("Spring Boot");
        adamWest.addTask("Hibernate");
        annaNowak.addTask("For loop");

        //Then
        assertThat(janKowalski.getUpdateCount()).isEqualTo(3);
        assertThat(robertKubica.getUpdateCount()).isEqualTo(4);
    }
}