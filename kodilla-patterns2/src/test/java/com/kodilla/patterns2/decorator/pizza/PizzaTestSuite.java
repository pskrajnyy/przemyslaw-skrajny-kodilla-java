package com.kodilla.patterns2.decorator.pizza;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class PizzaTestSuite {
    @Test
    public void testBasicPizzaOrderGetCost(){
        // Given
        PizzaOrder pizzaOrder = new BasicPizzaOrder();

        // When
        BigDecimal result = pizzaOrder.getCost();

        // Then
        assertEquals(new BigDecimal(10), result);
    }

    @Test
    public void testBasicPizzaOrderGetDescription(){
        // Given
        PizzaOrder pizzaOrder = new BasicPizzaOrder();

        // When
        String result = pizzaOrder.getDescription();

        // Then
        assertEquals("Dough + Cheese", result);
    }

    @Test
    public void testBasicPizzaWithChickenWithSalamiGetCost(){
        // Given
        PizzaOrder pizzaOrder = new BasicPizzaOrder();
        pizzaOrder = new ChickenDecorator(pizzaOrder);
        pizzaOrder = new SalamiDecorator(pizzaOrder);

        // When
        BigDecimal result = pizzaOrder.getCost();

        // Then
        assertEquals(new BigDecimal(20), result);
    }

    @Test
    public void testBasicPizzaWithChickenWithSalamiGetDescription(){
        // Given
        PizzaOrder pizzaOrder = new BasicPizzaOrder();
        pizzaOrder = new ChickenDecorator(pizzaOrder);
        pizzaOrder = new SalamiDecorator(pizzaOrder);

        // When
        String result = pizzaOrder.getDescription();

        // Then
        assertEquals("Dough + Cheese + Chicken + Salami", result);
    }

    @Test
    public void testBasicPizzaDeliverWithTankGetCost(){
        // Given
        PizzaOrder pizzaOrder = new BasicPizzaOrder();
        pizzaOrder.getClass();

        System.out.print(PizzaOrder.class);

        pizzaOrder = new DeliverByTankDecorator(pizzaOrder);

        // When
        BigDecimal result = pizzaOrder.getCost();

        // Then
        assertEquals(new BigDecimal(20_010), result);
    }

    @Test
    public void testBasicPizzaDeliverWithTankGetDescription(){
        // Given
        PizzaOrder pizzaOrder = new BasicPizzaOrder();
        pizzaOrder = new DeliverByTankDecorator(pizzaOrder);

        // When
        String result = pizzaOrder.getDescription();

        // Then
        assertEquals("Dough + Cheese Deliver by tank", result);
    }

    @Test
    public void testBasicPizzaDeliverWithHelieGetCost(){
        // Given
        PizzaOrder pizzaOrder = new BasicPizzaOrder();
        pizzaOrder = new DeliverByHeliDecorator(pizzaOrder);

        // When
        BigDecimal result = pizzaOrder.getCost();

        // Then
        assertEquals(new BigDecimal(20010), result);
    }

    @Test
    public void testBasicPizzaDeliverWithHelitDescription(){
        // Given
        PizzaOrder pizzaOrder = new BasicPizzaOrder();
        pizzaOrder = new DeliverByHeliDecorator(pizzaOrder);

        // When
        String result = pizzaOrder.getDescription();

        // Then
        assertEquals("Dough + Cheese Deliver by helicopter", result);
    }
}