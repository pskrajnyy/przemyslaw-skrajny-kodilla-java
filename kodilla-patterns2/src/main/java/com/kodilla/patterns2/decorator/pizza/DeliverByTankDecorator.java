package com.kodilla.patterns2.decorator.pizza;

import java.math.BigDecimal;

public class DeliverByTankDecorator extends AbstractPizzaOrderDecorator {
    public DeliverByTankDecorator(PizzaOrder pizzaOrder) {
        super(pizzaOrder);
    }

    @Override
    public BigDecimal getCost() {
        return super.getCost().add(new BigDecimal(20_000));
    }

    @Override
    public String getDescription() {
        return super.getDescription() + " Deliver by tank";
    }
}