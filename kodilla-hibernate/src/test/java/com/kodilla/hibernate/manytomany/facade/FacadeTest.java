package com.kodilla.hibernate.manytomany.facade;

import com.kodilla.hibernate.manytomany.Company;
import com.kodilla.hibernate.manytomany.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FacadeTest {
    @Autowired
    private Facade facade;

    @Test
    public void company() {
        //Given
        Company softwareMachine = new Company("Software Machine");
        Company dataMaesters = new Company("Data Maesters");
        Company greyMatter = new Company("Grey Matter");
        facade.saveCompany(softwareMachine);
        int softwareMachineId = softwareMachine.getId();
        facade.saveCompany(dataMaesters);
        int dataMaestersId = dataMaesters.getId();
        facade.saveCompany(greyMatter);
        int greyMatterId = greyMatter.getId();

        //When
        List<Company> result = facade.findCompanyByPartOfName("Sof");
        List<Company> result1 = facade.findCompanyByPartOfName("war");

        //Then
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(1, result1.size());

        //CleanUp
        try {
            facade.deleteCompany(softwareMachineId);
            facade.deleteCompany(dataMaestersId);
            facade.deleteCompany(greyMatterId);
        } catch (Exception e) {

        }
    }

    @Test
    public void employee() {
        //Given
        Employee johnSmith = new Employee("John", "Smith");
        Employee stephanieClarckson = new Employee("Stephanie", "Clarckson");
        Employee lindaKovalsky = new Employee("Linda", "Kovalsky");
        facade.saveEmployee(johnSmith);
        int johnId = johnSmith.getId();
        facade.saveEmployee(stephanieClarckson);
        int stephanieId = stephanieClarckson.getId();
        facade.saveEmployee(lindaKovalsky);
        int lindaId = lindaKovalsky.getId();

        //When
        List<Employee> result = facade.findEmployeeByPartLastname("mit");
        List<Employee> result1 = facade.findEmployeeByPartLastname("Cla");

        //Then
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(1, result1.size());

        //CleanUp
        try {
            facade.deleteEmployee(johnId);
            facade.deleteEmployee(stephanieId);
            facade.deleteEmployee(lindaId);
        } catch (Exception e) {

        }
    }
}