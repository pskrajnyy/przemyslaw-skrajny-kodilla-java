package com.kodilla.hibernate.manytomany.facade;

import com.kodilla.hibernate.manytomany.Company;
import com.kodilla.hibernate.manytomany.Employee;
import com.kodilla.hibernate.manytomany.dao.CompanyDao;
import com.kodilla.hibernate.manytomany.dao.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Facade {
    @Autowired
    private CompanyDao companyDao;
    @Autowired
    private EmployeeDao employeeDao;

    public void saveCompany(Company company) {
        companyDao.save(company);
    }

    public void saveEmployee(Employee employee) {
        employeeDao.save(employee);
    }

    public void deleteCompany(int companyId) {
        companyDao.deleteById(companyId);
    }

    public void deleteEmployee(int employeeId) {
        employeeDao.deleteById(employeeId);
    }

    public List<Employee> findEmployeeByPartLastname(final String partLastname) {
        return employeeDao.findByPartLastname2(partLastname);
    }

    public List<Company> findCompanyByPartOfName(final String partOfName) {
        return companyDao.findByPartName2(partOfName);
    }
}