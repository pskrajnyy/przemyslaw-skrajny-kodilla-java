package com.kodilla.hibernate.manytomany;

;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NamedQuery(
        name = "Company.findByName",
        query = "FROM Company WHERE name = :NAME"
)

@NamedNativeQuery(
        name = "Company.findByPartName",
        query = "SELECT * FROM Companies WHERE Company_Name LIKE CONCAT(:PART, '%')",
        resultClass = Company.class
)

@NamedNativeQuery(
        name = "Company.findByPartName2",
        query = "SELECT * FROM Companies WHERE Company_Name LIKE CONCAT('%', :PART, '%')",
        resultClass = Company.class
)
@Entity
@Table(name = "COMPANIES")
public class Company {
    private int id;
    private String name;
    private List<Employee> employees = new ArrayList<>();

    public Company() {

    }

    public Company(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue
    @Column(name = "COMPANY_ID", unique = true)
    public int getId() {
        return id;
    }


    @Column(name = "COMPANY_NAME")
    public String getName() {
        return name;
    }

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "companies")
    public List<Employee> getEmployees() {
        return employees;
    }

    private void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    private void setId(int id) {
        this.id = id;
    }

    private void setName(String name) {
        this.name = name;
    }
}