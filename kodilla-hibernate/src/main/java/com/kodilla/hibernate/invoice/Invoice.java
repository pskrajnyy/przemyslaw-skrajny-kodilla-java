package com.kodilla.hibernate.invoice;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "INVOICE")
public class Invoice {
    private int ID;
    private List<Item> items;
    private String number;

    public Invoice(List<Item> items, String number) {
        this.items = items;
        this.number = number;
    }

    public Invoice() {
    }

    @Id
    @GeneratedValue
    @Column(name = "ID", unique = true)
    public int getID() {
        return ID;
    }

    @OneToMany(
            targetEntity = Item.class,
            mappedBy = "invoice",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            fetch = FetchType.LAZY
    )
    public List<Item> getItems() {
        return items;
    }

    @Column(name = "NUMBER")
    public String getNumber() {
        return number;
    }

    private void setID(int ID) {
        this.ID = ID;
    }

    private void setItems(List<Item> items) {
        this.items = items;
    }

    private void setNumber(String number) {
        this.number = number;
    }
}
