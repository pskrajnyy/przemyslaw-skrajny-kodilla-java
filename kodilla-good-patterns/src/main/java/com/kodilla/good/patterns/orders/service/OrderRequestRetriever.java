package com.kodilla.good.patterns.orders.service;

import com.kodilla.good.patterns.orders.model.Order;
import com.kodilla.good.patterns.orders.model.OrderRequest;
import com.kodilla.good.patterns.orders.model.User;

public class OrderRequestRetriever {

    public OrderRequest retrieve() {

        User user = new User("Przemysław", "Skrajny");

        Order order1 = new Order("Kilogram ziemniakow");
        Order order2 = new Order("Mleko");

        return new OrderRequest(user, order1, order2);
    }
}
