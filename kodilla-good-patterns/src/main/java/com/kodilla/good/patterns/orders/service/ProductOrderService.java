package com.kodilla.good.patterns.orders.service;

import com.kodilla.good.patterns.orders.model.Order;
import com.kodilla.good.patterns.orders.model.User;
import com.kodilla.good.patterns.orders.interfaces.OrderService;

import java.util.List;

public class ProductOrderService implements OrderService {

    @Override
    public boolean checkAvailable(User user, List<Order> order) {
        System.out.println("Checking if possible to buy: " + order.toString() + " for " + user.toString());
        return true;
    }
}
