package com.kodilla.good.patterns.orders.service;

import com.kodilla.good.patterns.orders.model.Order;
import com.kodilla.good.patterns.orders.model.OrderRequest;
import com.kodilla.good.patterns.orders.model.User;
import com.kodilla.good.patterns.orders.dto.OrderDto;
import com.kodilla.good.patterns.orders.interfaces.InformationService;
import com.kodilla.good.patterns.orders.interfaces.OrderRepository;
import com.kodilla.good.patterns.orders.interfaces.OrderService;

import java.util.List;

public class OrderProcessor {

    private InformationService informationService;
    private OrderService orderService;
    private OrderRepository orderRepository;

    public OrderProcessor(final InformationService informationService, final OrderService orderService, final OrderRepository orderRepository) {
        this.informationService = informationService;
        this.orderService = orderService;
        this.orderRepository = orderRepository;
    }

    public OrderDto process(final OrderRequest orderRequest) {

        User user = orderRequest.getUser();
        List<Order> orderList = orderRequest.getOrderList();

        boolean isAvailable = orderService.checkAvailable(user, orderList);

        if (isAvailable) {
            informationService.inform(user);
            orderRepository.createPurchase(user, orderList);
            return new OrderDto(user, true);
        } else {
            return new OrderDto(user, false);
        }
    }
}