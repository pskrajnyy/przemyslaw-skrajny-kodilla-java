package com.kodilla.good.patterns.orders.service;

import com.kodilla.good.patterns.orders.model.User;
import com.kodilla.good.patterns.orders.interfaces.InformationService;

public class MailService implements InformationService {

    @Override
    public void inform(User user) {
        System.out.printf("Sending information e-mail for user: " + user.toString());
    }
}
