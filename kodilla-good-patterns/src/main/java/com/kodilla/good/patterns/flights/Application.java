package com.kodilla.good.patterns.flights;

import java.util.List;
import java.util.Map;

public class Application {
    public static void main(String[] args) {

        FlightsDatabase dataBase = new FlightsDatabase();
        Map<Airport, List<Airport>> airportsMap = dataBase.retrieve();
        FlightSeeker flightSeeker = new FlightSeeker(airportsMap);

        Airport wroclaw = new Airport("Wroclaw");
        System.out.println("Bezpośrednie polaczenia z Wroclawia: " + flightSeeker.findDirectConnectionsFrom(wroclaw));

        Airport gdansk = new Airport("Gdansk");
        System.out.println("\nBezposrednie polaczenia do Gdanska: " + flightSeeker.findDirectConnectionsTo(gdansk));


        System.out.println("\nPołączenia z Wrocławia do Gdańska: ");
        flightSeeker.findConnectionsFromTo(wroclaw, gdansk);

    }
}
