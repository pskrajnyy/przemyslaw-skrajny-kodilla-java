package com.kodilla.good.patterns.food2doors.service;

import com.kodilla.good.patterns.food2doors.products.ExtraFoodShopProduct;
import com.kodilla.good.patterns.food2doors.products.GlutenFreeShopProduct;
import com.kodilla.good.patterns.food2doors.products.HealthyShopProduct;

public class OrderRequestRetriever {

    public OrderRequest retrieve() {
        final Product product1 = new ExtraFoodShopProduct("Jablko", 5, 4);
        final Product product2 = new HealthyShopProduct("Mieso mielone", 4.5,0.5);
        final Product product3 = new GlutenFreeShopProduct("Parowki bezglutenowe", 7.89,10);

        return new OrderRequest(product1, product2, product3);
    }
}
