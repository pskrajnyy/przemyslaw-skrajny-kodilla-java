package com.kodilla.good.patterns.orders.repository;

import com.kodilla.good.patterns.orders.model.Order;
import com.kodilla.good.patterns.orders.model.User;
import com.kodilla.good.patterns.orders.interfaces.OrderRepository;

import java.util.List;

public class ProductOrderRepository implements OrderRepository {


    @Override
    public void createPurchase(User user, List<Order> order) {
        System.out.println("Creating a purchase for: " + user.toString() );
    }
}
