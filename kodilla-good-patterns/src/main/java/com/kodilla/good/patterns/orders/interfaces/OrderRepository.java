package com.kodilla.good.patterns.orders.interfaces;

import com.kodilla.good.patterns.orders.model.Order;
import com.kodilla.good.patterns.orders.model.User;

import java.util.List;

public interface OrderRepository {
    void createPurchase(User user, List<Order> order);
}
