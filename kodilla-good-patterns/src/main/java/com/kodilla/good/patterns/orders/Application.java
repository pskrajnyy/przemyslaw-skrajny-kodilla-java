package com.kodilla.good.patterns.orders;

import com.kodilla.good.patterns.orders.model.OrderRequest;
import com.kodilla.good.patterns.orders.repository.ProductOrderRepository;
import com.kodilla.good.patterns.orders.service.MailService;
import com.kodilla.good.patterns.orders.service.OrderProcessor;
import com.kodilla.good.patterns.orders.service.OrderRequestRetriever;
import com.kodilla.good.patterns.orders.service.ProductOrderService;

public class Application {

    public static void main(String[] args) {

        OrderRequestRetriever orderRequestRetriever = new OrderRequestRetriever();
        OrderRequest orderRequest = orderRequestRetriever.retrieve();

        OrderProcessor OrderProcessor = new OrderProcessor(new MailService(), new ProductOrderService(), new ProductOrderRepository());

        OrderProcessor.process(orderRequest);
    }
}