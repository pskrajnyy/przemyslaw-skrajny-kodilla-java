package com.kodilla.good.patterns.orders.model;

import com.kodilla.good.patterns.orders.model.User;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrderRequest {

    private User user;
    private List<Order> orderList = new ArrayList<>();

    public OrderRequest(User user, Order... order) {
        this.user = user;
        this.orderList.addAll(Arrays.asList(order));
    }

    public User getUser() {
        return user;
    }

    public List<Order> getOrderList() {
        return new ArrayList<>(orderList);
    }
}
