package com.kodilla.good.patterns.food2doors.products;

import com.kodilla.good.patterns.food2doors.service.Product;

public class HealthyShopProduct extends Product {

    public HealthyShopProduct(String name, double price, double quantity) {
        super(name, price, quantity);
    }
}
