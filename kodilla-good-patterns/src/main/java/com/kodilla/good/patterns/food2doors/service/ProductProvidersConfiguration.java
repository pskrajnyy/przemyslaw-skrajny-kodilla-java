package com.kodilla.good.patterns.food2doors.service;

import com.kodilla.good.patterns.food2doors.shops.ExtraFoodShop;
import com.kodilla.good.patterns.food2doors.shops.GlutenFreeShop;
import com.kodilla.good.patterns.food2doors.shops.HealthyShop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductProvidersConfiguration {
    public static List<ProductProvider> buildProductProviders() {
        return new ArrayList<>(Arrays.asList(new HealthyShop(), new ExtraFoodShop(), new GlutenFreeShop()));
    }
}
