package com.kodilla.good.patterns.orders.interfaces;

import com.kodilla.good.patterns.orders.model.Order;
import com.kodilla.good.patterns.orders.model.User;

import java.util.List;

public interface OrderService {
    boolean checkAvailable(User user, List<Order> order);
}
