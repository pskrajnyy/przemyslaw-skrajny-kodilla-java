package com.kodilla.good.patterns.orders.interfaces;

import com.kodilla.good.patterns.orders.model.User;

public interface InformationService {
    void inform(User user);
}
