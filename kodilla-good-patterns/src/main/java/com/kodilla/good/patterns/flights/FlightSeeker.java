package com.kodilla.good.patterns.flights;

import java.util.*;
import java.util.stream.Collectors;

public class FlightSeeker {
    private final Map<Airport, List<Airport>> airportsMap;

    public FlightSeeker(Map<Airport, List<Airport>> airportsMap) {
        this.airportsMap = airportsMap;
    }

    public Set<String> findDirectConnectionsFrom(Airport airport) {
        return airportsMap.get(airport).stream()
                .map(airport1 -> airport1.getAirportName())
                .collect(Collectors.toSet());
    }

    public Set<String> findDirectConnectionsTo(Airport airport) {
        return airportsMap.entrySet().stream()
                .filter(airportListEntry -> airportListEntry.getValue().contains(airport))
                .map(entry -> entry.getKey())
                .map(airport1 -> airport1.getAirportName())
                .collect(Collectors.toSet());
    }

    public Set<String> findConnectionsFromTo(Airport airportFrom, Airport airportTo) {
        Set<String> result = new HashSet<>();

        List<String> list1 = airportsMap.entrySet().stream()
                .filter(airportListEntry -> airportListEntry.getValue().contains(airportTo))
                .map(Map.Entry::getKey)
                .map(s -> s.getAirportName())
                .collect(Collectors.toList());

        List<String> list2 = airportsMap.get(airportFrom).stream()
                .map(s -> s.getAirportName())
                .collect(Collectors.toList());

        for (String list : list2) {
            if (list.contains(airportTo.getAirportName())) {
                result.add("Bezposrednio");
            } else if (list1.contains(list)) {
                result.add(list);
            }
        }
        return result;
    }
}
