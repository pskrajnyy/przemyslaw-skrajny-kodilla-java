package com.kodilla.good.patterns.food2doors.products;

import com.kodilla.good.patterns.food2doors.service.Product;

public class ExtraFoodShopProduct extends Product {

    public ExtraFoodShopProduct(String name, double price, double quantity) {
        super(name, price, quantity);
    }
}
