package com.kodilla.patterns.builder.bigmac;

import org.junit.Assert;
import org.junit.Test;

public class BigmacTestSuite {
    @Test
    public void testBigmacNew() {
        //Given
        Bigmac bigmac = new Bigmac.BigmacBuilder()
                .bun("Thin")
                .burgers(3)
                .ingredient("cheese")
                .sauce("Spicy")
                .ingredient("ham")
                .build();
        //When
        int howManyIngredients = bigmac.getIngredients().size();
        int howManyBurgers = bigmac.getBurgers();
        //Then
        Assert.assertEquals(2, howManyIngredients);
        Assert.assertEquals(3,howManyBurgers);
        Assert.assertTrue(bigmac.getIngredients().contains("ham"));
        Assert.assertTrue(bigmac.getIngredients().contains("cheese"));
    }
}
