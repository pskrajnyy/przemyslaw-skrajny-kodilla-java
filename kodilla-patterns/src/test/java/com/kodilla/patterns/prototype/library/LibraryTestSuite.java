package com.kodilla.patterns.prototype.library;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.IntStream;

public class LibraryTestSuite {

    @Test
    public void testGetBooks() {
        //Given
        Library library = new Library("Biblioteka 1");
        IntStream.iterate(1, n -> n+1 )
                .limit(5)
                .forEach(n -> library.getBooks().add(new Book(String.valueOf(n), "Ktos", LocalDate.now())));

        Library shallowClonedLibrary = null;
        try {
            shallowClonedLibrary = library.shallowCopy();
            shallowClonedLibrary.setName("Biblioteka 'Shallow'");
        } catch (CloneNotSupportedException e) {
            System.out.println(e);
        }

        Library deepClonedLibrary = null;
        try {
            deepClonedLibrary = library.deepClone();
            deepClonedLibrary.setName("Biblioteka 'Deep'");
        } catch (CloneNotSupportedException e) {
            System.out.println(e);
        }

        System.out.println(library);
        System.out.println(deepClonedLibrary);
        System.out.println(shallowClonedLibrary);
        //When
        System.out.println("===============================================================");
        library.getBooks().add(new Book("DODATKOWA", "DODATKOWA", LocalDate.now()));
        //Then
        System.out.println(library);
        System.out.println(deepClonedLibrary);
        System.out.println(shallowClonedLibrary);
        Assert.assertEquals(6, library.getBooks().size());
        Assert.assertEquals(5, deepClonedLibrary.getBooks().size());
        Assert.assertEquals(6,shallowClonedLibrary.getBooks().size());
    }
}
