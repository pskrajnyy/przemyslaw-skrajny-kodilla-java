package com.kodilla.patterns.strategy.social;

import com.kodilla.patterns.strategy.social.mediums.FacebookPublisher;
import org.junit.Assert;
import org.junit.Test;

public class UserTestSuite {

    @Test
    public void testDefaultSharingStrategies() {
        //Given
        User patrycja = new ZGeneration("Patrycja");
        User przemek = new YGeneration("Przemek");
        User klaudia = new Millenials("Klaudia");

        //When
        System.out.println("Patrycja: " + patrycja.share());
        System.out.println("Przemek: " + przemek.share());
        System.out.println("Klaudia: " + klaudia.share());

        //Then
        Assert.assertEquals("Snapchat", patrycja.share());
        Assert.assertEquals("Twitter", przemek.share());
        Assert.assertEquals("Facebook", klaudia.share());
    }

    @Test
    public void testIndividualSharingStrategy() {
        //Given
        User patrycja = new ZGeneration("Patrycja");

        //When
        System.out.println("Patrycja: " + patrycja.share());
        patrycja.setSocialPublisher(new FacebookPublisher());
        System.out.println("Patrycja: " + patrycja.share());

        //Then
        Assert.assertEquals("Facebook", patrycja.share());
    }
}
