package com.kodilla.spring.portfolio;

import javafx.concurrent.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

public class BoardConfig {

    @Autowired
    @Qualifier("toDo")
    private TaskList toDo;

    @Autowired
    @Qualifier("inProgress")
    private TaskList inProgress;

    @Autowired
    @Qualifier("done")
    private TaskList done;

    @Bean
    public Board getBoard() {
        return new Board(toDo, inProgress, done);
    }

    @Bean(name = "toDo")
    @Scope("prototype")
    public TaskList getTaskListToDo() {
        return new TaskList();
    }

    @Bean(name = "inProgress")
    @Scope("prototype")
    public TaskList getTaskListInProgress() {
        return new TaskList();
    }

    @Bean(name = "done")
    @Scope("prototype")
    public TaskList getTaskListDone() {
        return new TaskList();
    }
}
