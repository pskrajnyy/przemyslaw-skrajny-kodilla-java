package com.kodilla.spring.portfolio;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(value = "spring.main.allow-bean-definition-overriding=true")
public class PortfolioTaskSuite {

    @Test
    public void testShowBoard(){
        ApplicationContext context = new AnnotationConfigApplicationContext(BoardConfig.class);
        Board board = context.getBean(Board.class);

        //When&Then
        System.out.println(board.toString());
    }

    @Test
    public void testAddOneTask(){
        ApplicationContext context = new AnnotationConfigApplicationContext(BoardConfig.class);
        Board board = context.getBean(Board.class);

        //When
        board.getToDoList().addTask("Powtorzyc material");
        board.getInProgressList().addTask("Wyslac do mentora zadanie 4");
        board.getInProgressList().addTask("Isc do dentysty");
        board.getDoneList().addTask("Zamowic jedzenie");
        //Then
        System.out.println(board.toString());

        Assert.assertEquals(1, board.getToDoList().getTasks().size());
        Assert.assertEquals(2, board.getInProgressList().getTasks().size());
        Assert.assertEquals(1, board.getDoneList().getTasks().size());
    }
}
