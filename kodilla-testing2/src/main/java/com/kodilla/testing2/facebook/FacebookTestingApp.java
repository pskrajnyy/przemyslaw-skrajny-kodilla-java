package com.kodilla.testing2.facebook;

import com.kodilla.testing2.config.WebDriverConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class FacebookTestingApp {

    public static final String XPATH_FIRSTNAME = "//div[contains(@class, \"uiStickyPlaceholderEmptyInput\")]/input";
    public static final String XPATH_LASTNAME = "//div[contains(@class, \"mbm _1iy_ _a4y rfloat _ohf\")]/div/div/input";
    public static final String XPATH_EMAIL = "//div[contains(@class, \"uiStickyPlaceholderInput uiStickyPlaceholderEmptyInput\")]/input";
    public static final String CONFIRMATION_EMAIL = "u_0_w";
    public static final String XPATH_DAY = "//div[contains(@class, \"_5k_5\")]/span/span/select";
    public static final String XPATH_MONTH = "//div[contains(@class, \"_5k_5\")]/span/span/select[2]";
    public static final String XPATH_YEAR = "//div[contains(@class, \"_5k_5\")]/span/span/select[3]";

    public static void main(String[] args) {
        WebDriver webDriver = WebDriverConfig.getDriver(WebDriverConfig.FIREFOX);
        webDriver.get("https://facebook.com");

        WebElement firstnameField = webDriver.findElement(By.xpath(XPATH_FIRSTNAME));
        firstnameField.sendKeys("Przemyslaw");

        WebElement lastnameField = webDriver.findElement(By.xpath(XPATH_LASTNAME));
        lastnameField.sendKeys("Skrajny");

        WebElement emailField = webDriver.findElement(By.xpath(XPATH_EMAIL));
        emailField.sendKeys("testowy@gmail.com");

       while(!webDriver.findElement(By.id(CONFIRMATION_EMAIL)).isDisplayed());

        WebElement confirmationEmail = webDriver.findElement(By.id(CONFIRMATION_EMAIL));
        confirmationEmail.sendKeys("testowy@gmail.com");

        WebElement selectDayCombo = webDriver.findElement(By.xpath(XPATH_DAY));
        Select selectDay = new Select(selectDayCombo);
        selectDay.selectByIndex(14);

        WebElement selectMonthCombo = webDriver.findElement(By.xpath(XPATH_MONTH));
        Select selectMonth = new Select(selectMonthCombo);
        selectMonth.selectByIndex(11);

        WebElement selectYearCombo = webDriver.findElement(By.xpath(XPATH_YEAR));
        Select selectYear = new Select(selectYearCombo);
        selectYear.selectByIndex(1);
    }
}
