package com.kodilla.testing.forum.statistics;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StatisticsCalculatorTest {

    @Test
    public void calculateAdvStatistics() {
        //Given
        Statistics statistics = mock(Statistics.class);
        StatisticsCalculator statisticsCalculator = new StatisticsCalculator();
        when(statistics.usersNames()).thenReturn(Arrays.asList("Jan","Pawel"));
        when(statistics.commentsCounts()).thenReturn(10);
        when(statistics.postsCounts()).thenReturn(5);
        //When
        statisticsCalculator.calculateAdvStatistics(statistics);
        //Then
        Assert.assertEquals(10,statisticsCalculator.getComments());
        Assert.assertEquals(5,statisticsCalculator.getPosts());
        Assert.assertEquals(2,statisticsCalculator.users);
        Assert.assertEquals(2,statisticsCalculator.getCommentsPerPost(),0.1);
        Assert.assertEquals(5,statisticsCalculator.getCommentsPerUser(),0.1);
        Assert.assertEquals(2.5,statisticsCalculator.getPostsPerUser(),0.1);
    }

    @Test
    public void calculateAdvStatisticsByZero() {
        //Given
        Statistics statistics = mock(Statistics.class);
        StatisticsCalculator statisticsCalculator = new StatisticsCalculator();
        when(statistics.usersNames()).thenReturn(Arrays.asList("Jan","Pawel"));
        when(statistics.commentsCounts()).thenReturn(5);
        when(statistics.postsCounts()).thenReturn(0);
        //When
        statisticsCalculator.calculateAdvStatistics(statistics);
        //Then
        Assert.assertEquals(5,statisticsCalculator.getComments());
        Assert.assertEquals(0,statisticsCalculator.getPosts());
        Assert.assertEquals(2,statisticsCalculator.users);
        Assert.assertEquals(0,statisticsCalculator.getCommentsPerPost(),0.1);
        Assert.assertEquals(2.5,statisticsCalculator.getCommentsPerUser(),0.1);
        Assert.assertEquals(0,statisticsCalculator.getPostsPerUser(),0.1);
    }
}