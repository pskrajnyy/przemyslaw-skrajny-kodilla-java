package com.kodilla.testing.collection;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class CollectionTestSuite {
    @Before
    public void before(){
        System.out.println("Test Case: begin");
    }

    @After
    public void after() {
        System.out.println("Test Case: end");
    }

    @Test
    public void testOddNumberExterminatorNormalList() {
        //Given
        ArrayList<Integer> numbers = new ArrayList<>();
        ArrayList<Integer> numbersWithoutOdd = new ArrayList<>();
        OddNumbersExterminator oddNumbersExterminator = new OddNumbersExterminator();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);
        numbers.add(7);
        numbersWithoutOdd.add(2);
        numbersWithoutOdd.add(4);
        numbersWithoutOdd.add(6);
        //When
        ArrayList<Integer> result = oddNumbersExterminator.exterminate(numbers);
        //Then
        Assert.assertEquals(result,numbersWithoutOdd);
    }

    @Test
    public void testOddNumberExterminatorEmptyList() {
        //Given
        ArrayList<Integer> numbers = new ArrayList<>();
        ArrayList<Integer> emptyList = new ArrayList<>();
        OddNumbersExterminator oddNumbersExterminator = new OddNumbersExterminator();
        //When
        ArrayList<Integer> result = oddNumbersExterminator.exterminate(numbers);
        //Then
        Assert.assertEquals(result,emptyList);
    }
}
