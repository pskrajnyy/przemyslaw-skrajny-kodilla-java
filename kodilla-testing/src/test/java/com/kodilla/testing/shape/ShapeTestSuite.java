package com.kodilla.testing.shape;

import org.junit.*;

public class ShapeTestSuite {

    private static int testCounter = 0;

    @BeforeClass
    public static void beforeAllTests() {
        System.out.println("This is the beginning of tests.");
    }

    @AfterClass
    public static void afterAllTests() {
        System.out.println("All tests are finished");
    }

    @Before
    public void beforeEveryTest() {
        testCounter++;
        System.out.println("Preparing to execute test #" + testCounter);
    }

    @Test
    public void testFieldCircle() {
        //Given
        Circle circle = new Circle(5.0);
        //When
        double result = circle.getField();
        //Then
        Assert.assertEquals(78.54, result,0.1);
    }

    @Test
    public void testFieldSquare() {
        //Given
        Square square = new Square(5.0);
        //When
        double result = square.getField();
        //Then
        Assert.assertEquals(25, result,0.1);
    }

    @Test
    public void testFieldTriangle() {
        //Given
        Triangle triangle = new Triangle(10,10);
        //When
        double result = triangle.getField();
        //Then
        Assert.assertEquals(50, result,0.1);
    }

    @Test
    public void testAddShape() {
        //Given
        Circle circle = new Circle(5.0);
        ShapeCollector shapeCollector = new ShapeCollector();
        //When
        shapeCollector.addFigure(circle);
        //Then
        Assert.assertEquals(1, shapeCollector.numbersShapes());
    }

    @Test
    public void testRemoveShape() {
        //Given
        Circle circle = new Circle(5.0);
        ShapeCollector shapeCollector = new ShapeCollector();
        shapeCollector.addFigure(circle);
        //When
        boolean result = shapeCollector.removeFigure(circle);
        //Then
        Assert.assertTrue(result);
        Assert.assertEquals(0, shapeCollector.numbersShapes());
    }

    @Test
    public void testRemoveShapeNotExisting() {
        //Given
        Circle circle = new Circle(5.0);
        Triangle triangle = new Triangle(10, 5);
        ShapeCollector shapeCollector = new ShapeCollector();
        shapeCollector.addFigure(circle);
        //When
        boolean result = shapeCollector.removeFigure(triangle);
        //Then
        Assert.assertFalse(result);
    }

    @Test
    public void testGetFigure() {
        //Given
        Circle circle = new Circle(5.0);
        ShapeCollector shapeCollector = new ShapeCollector();
        shapeCollector.addFigure(circle);
        //When
        Shape result = shapeCollector.getFigure(0);
        //Then
        Assert.assertSame(result, circle);
    }
}
