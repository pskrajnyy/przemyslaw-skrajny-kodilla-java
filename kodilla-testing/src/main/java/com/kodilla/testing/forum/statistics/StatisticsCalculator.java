package com.kodilla.testing.forum.statistics;

public class StatisticsCalculator {

    int users;
    int posts;
    int comments;
    double postsPerUser;
    double commentsPerUser;
    double commentsPerPost;

    public void calculateAdvStatistics(Statistics statistics) {
        users = statistics.usersNames().size();
        posts = statistics.postsCounts();
        comments = statistics.commentsCounts();
        postsPerUser = users == 0 ? 0 : (double) posts / users;
        commentsPerUser = users == 0 ? 0 : (double) comments / users;
        commentsPerPost = posts == 0 ? 0 : (double) comments / posts;
    }

    public int getUsers() {
        return users;
    }

    public int getPosts() {
        return posts;
    }

    public int getComments() {
        return comments;
    }

    public double getPostsPerUser() {
        return postsPerUser;
    }

    public double getCommentsPerUser() {
        return commentsPerUser;
    }

    public double getCommentsPerPost() {
        return commentsPerPost;
    }
}
