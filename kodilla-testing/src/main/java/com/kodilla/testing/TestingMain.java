package com.kodilla.testing;

import com.kodilla.testing.user.SimpleUser;
import com.kodilla.testing.calculator.Calculator;

public class TestingMain {
    public static void main(String[] args) {
        SimpleUser simpleUser = new SimpleUser("theForumUser", "John Smith");

        String result = simpleUser.getUsername();

        if(result.equals("theForumUser")) {
            System.out.println("test OK");
        } else {
            System.out.println("Error!");
        }

        //Tutaj nastepuje testowanie klasy Calcualator
        Calculator calculator = new Calculator();

        if(calculator.add(25,13) == 39) {
            System.out.println("add - OK!");
        } else {
            System.out.println("add - ERROR!");
        }

        if(calculator.subtract(25,10) == 15) {
            System.out.println("subtract - OK!");
        } else {
            System.out.println("subtract - ERROR!");
        }
    }
}
