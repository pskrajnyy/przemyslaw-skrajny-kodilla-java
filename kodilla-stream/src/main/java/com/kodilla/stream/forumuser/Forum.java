package com.kodilla.stream.forumuser;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public final class Forum {
    private final List<ForumUser> theUserList = new ArrayList<>();

    public Forum() {
        theUserList.add(new ForumUser(0,"Przemek",'M',10, LocalDate.of(1994,10,12)));
        theUserList.add(new ForumUser(1, "Klaudia",'F',0, LocalDate.of(2000,10,12)));
        theUserList.add(new ForumUser(2,"Beata",'F',15, LocalDate.of(2001,10,12)));
        theUserList.add(new ForumUser(3, "Sławek",'M',1, LocalDate.of(1991,10,12)));
        theUserList.add(new ForumUser(4,"Michał",'M',23, LocalDate.of(1983,10,12)));
        theUserList.add(new ForumUser(5, "Piotrek",'M',4, LocalDate.of(2012,10,12)));
        theUserList.add(new ForumUser(6,"StudentPG",'M',0, LocalDate.of(1842,10,12)));
        theUserList.add(new ForumUser(7, "Sąsiad",'M',123, LocalDate.of(1984,10,12)));
        theUserList.add(new ForumUser(8,"Patrycja",'F',7, LocalDate.of(2001,10,12)));
        theUserList.add(new ForumUser(9, "Klaudynka",'F',0, LocalDate.of(2016,10,12)));
    }

    public List<ForumUser> getTheUserList() {
        return new ArrayList<>(theUserList);
    }
}
