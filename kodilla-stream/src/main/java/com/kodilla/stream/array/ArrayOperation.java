package com.kodilla.stream.array;

import java.util.stream.IntStream;

public interface ArrayOperation {

    static double getAverage(int[] numbers){

        if(numbers.length==0) return 0;

        IntStream.range(0, numbers.length)
                .map(n -> numbers[n])
                .forEach(System.out::println);

        Double averange = IntStream.range(0, numbers.length)
                .map(n -> numbers[n])
                .average().getAsDouble();

        return averange;
    }
}
