package com.kodilla.stream.world;

import org.junit.Assert;
import org.junit.Test;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class WorldTestSuite {
    @Test
    public void testGetPeopleQuantity() {
        //Given
        Country Poland = new Country("Poland", new BigDecimal(1_000_000));
        Country Ukraine = new Country("Ukraine", new BigDecimal(2_000_000));
        Country China = new Country("China", new BigDecimal(3_000_000));
        Country Russia = new Country("Russia", new BigDecimal(5_000_000));
        Country Chad = new Country("Chad", new BigDecimal(3_000_000));
        Country Egypt = new Country("Egypt", new BigDecimal(1_000_000));
        List<Country> europaList = new ArrayList<>();
        List<Country> asiaList = new ArrayList<>();
        List<Country> africaList = new ArrayList<>();
        Continent Europe = new Continent(europaList,"Europe");
        Continent Asia = new Continent(asiaList, "Asia");
        Continent Africa = new Continent(africaList, "Africa");
        List<Continent> continentList = new ArrayList<>();
        continentList.add(Europe);
        continentList.add(Asia);
        continentList.add(Africa);

        //When
        europaList.add(Poland);
        europaList.add(Ukraine);
        asiaList.add(China);
        asiaList.add(Russia);
        africaList.add(Chad);
        africaList.add(Egypt);
        BigDecimal totalPeople = continentList.stream()
                .flatMap(continent -> continent.getCountryList().stream())
                .map(Country::getPeopleQuantity)
                .reduce(BigDecimal.ZERO,(sum, current) -> sum = sum.add(current));

        //Then
        BigDecimal expectedPeople = new BigDecimal(15_000_000);
        Assert.assertEquals(expectedPeople, totalPeople);
    }
}
