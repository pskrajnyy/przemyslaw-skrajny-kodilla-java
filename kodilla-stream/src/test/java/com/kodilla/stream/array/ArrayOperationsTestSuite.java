package com.kodilla.stream.array;

import org.junit.Assert;
import org.junit.Test;

public class ArrayOperationsTestSuite {

    @Test
    public void testGetAverage() {
        //Given
        int[] numbers = {2,4,6,8,10,8,6,4,2,0};

        //When
        double result = ArrayOperation.getAverage(numbers);

        //Then
        Assert.assertEquals(5, result, 0.001);
    }

    @Test
    public void testGetAverageZeroElements() {
        //Given
        int[] numbers = new int[0];

        //When
        double result = ArrayOperation.getAverage(numbers);

        //Then
        Assert.assertEquals(0,result,0.001);
    }
}
